use bevy::prelude::*;
use heron::prelude::*;

/// Assets for the pin. Must be used as a resource.
pub struct PinAssets {
    /// Label
    pub label: String,
    /// Scene
    pub scene: Handle<Scene>,
    /// Collider
    pub collider: CollisionShape,
    /// Body
    pub body: RigidBody,
    /// PhysicMaterial
    pub physics_material: PhysicMaterial,
}

impl PinAssets {
}
