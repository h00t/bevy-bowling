use bevy::prelude::*;
use heron::prelude::*;
use bevy_flycam::{PlayerPlugin, MovementSettings};
use bevy_inspector_egui::{WorldInspectorPlugin, egui::Shape};

use std::f32::consts::PI;

fn main() {
    App::new()
    .insert_resource(WindowDescriptor {
        width: 1920.,
        height: 1080.,
        vsync: true,
        ..Default::default()
    })
    .insert_resource(Msaa { samples: 4 })
    .add_plugins(DefaultPlugins)
    .add_plugin(PhysicsPlugin::default())
    .insert_resource(Gravity::from(Vec3::new(0.0, -4.81, 0.0)))
    .add_plugin(PlayerPlugin)
    .add_plugin(WorldInspectorPlugin::new())
    .insert_resource(MovementSettings {
        sensitivity: 0.00008,
        speed: 8.0,
    })
    .add_startup_system(setup_pins)
    .add_startup_system(setup_ball)
    .add_startup_system(setup_lighting)
    .add_startup_system(setup_player_lane)
    .add_startup_system(setup_environment_lanes)
    .run();
}

#[derive(Component)]
struct Ball;

fn setup_ball(
    mut commands: Commands,
    asset_server: Res<AssetServer>
) {
    // Place ball
    let ball = asset_server.load("ball/ball.gltf#Scene0");
    commands.spawn_bundle((
        Ball,
        Transform::from_xyz(1.757, 0.134, 8.3),
        GlobalTransform::identity(),
        RigidBody::Dynamic,
        CollisionShape::Sphere { radius: 0.11 },
        Name::new("Bowling Ball"),
        Velocity::from_linear(Vec3::new(0.0, 0.0, -7.0)),
        PhysicMaterial {
            restitution: 0.2,
            friction: 0.1,
            density: 6.0,
        },
    ))
    .with_children(|parent| {
        parent.spawn_scene(ball);
    });

}

fn setup_pins(
    mut commands: Commands,
    asset_server: Res<AssetServer>
) {
    let pin =  asset_server.load("pin/pin.gltf#Scene0");

    let rows = 4;
    let row_offset = -9.0;
    let row_spacing = 0.15;

    let cols = 7;
    let col_offset = 0.33;
    let col_spacing = 0.15;


    let pin_height = 0.216;

    let mut num_pins = rows;

    for pin_row in 0..rows {
        let mut placed_pins = 0;
        let spaces_to_skip = pin_row;
        let even_row = pin_row % 2 == 0;
        let row_pos = row_offset + (pin_row as f32 * row_spacing);

        for pin_col in 0..cols {
            let col_pos = col_offset + (pin_col as f32 * col_spacing);
            let even_col = pin_col % 2 == 0;
            let mut place_pin = false;

            if (even_row && even_col) || (!even_row && !even_col) {
                if spaces_to_skip > 0 {
                    if pin_col >= spaces_to_skip && placed_pins < num_pins {
                            place_pin = true;
                    }
                } else {
                    place_pin = true;
                }
            }


            if place_pin {
                commands.spawn_bundle((
                    Transform::from_xyz(
                        col_pos,
                        pin_height,
                        row_pos,
                    ),
                    GlobalTransform::identity(),
                    RigidBody::Dynamic,
                    CollisionShape::Cylinder {
                        half_height: 0.19,
                        radius: 0.06,
                    },
                    PhysicMaterial {
                        restitution: 0.5,
                        density: 1.5,
                        friction: 0.2,
                    },
                    Name::new("Pin"),
                )).with_children(|parent| {
                    parent.spawn_scene(pin.clone());
                });

                placed_pins += 1;
            }

        }
        
        num_pins -= 1;
        println!("\n");
    }

}

fn setup_environment_lanes(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
    let lane_pattern_size = 3.314;
    let mut x_origin = lane_pattern_size;
    let lane_sets = 3;

    for _set in 0..lane_sets {
        let left_gutter_shim = asset_server.load("gutter_shim/gutter_shim.glb#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin, 0.039, 0.0),
            GlobalTransform::identity(),
            Name::new("left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(left_gutter_shim);
        });

        let left_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 0.127, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(left_gutter);
        });

        let lane = asset_server.load("lane/lane.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 0.775, 0.0, 0.0),
            GlobalTransform::identity(),
            RigidBody::Static,
            CollisionShape::Cuboid { 
                half_extends: Vec3::new(0.53, 0.028, 9.58),
                border_radius: None,
            },
            PhysicMaterial {
                restitution: 0.5,
                ..Default::default()
            },
            Name::new("Lane")
        )).with_children(|parent| {
            parent.spawn_scene(lane);
        });

        let right_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 1.422, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("right gutter")
        )).with_children(|parent| {
            parent.spawn_scene(right_gutter);
        });

        let ball_return = asset_server.load("ball_return/ball_return.glb#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 1.657, 0.014, 0.0),
            GlobalTransform::identity(),
            Name::new("ball return")
        )).with_children(|parent| {
            parent.spawn_scene(ball_return);
        });

        // next lane - left gutter
        let next_left_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 1.892, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("next left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(next_left_gutter);
        });

        // next lane
        let next_lane = asset_server.load("lane/lane.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 2.54, 0.0, 0.0),
            GlobalTransform::identity(),
            Name::new("next lane")
        )).with_children(|parent| {
            parent.spawn_scene(next_lane);
        });

        // next lane - right gutter
        let next_right_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 3.187, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("next left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(next_right_gutter);
        });

        x_origin += lane_pattern_size;
    }

    x_origin = -lane_pattern_size;

    for _set in 0..lane_sets {
        let left_gutter_shim = asset_server.load("gutter_shim/gutter_shim.glb#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin, 0.039, 0.0),
            GlobalTransform::identity(),
            Name::new("left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(left_gutter_shim);
        });

        let left_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 0.127, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(left_gutter);
        });

        let lane = asset_server.load("lane/lane.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 0.775, 0.0, 0.0),
            GlobalTransform::identity(),
            Name::new("Lane")
        )).with_children(|parent| {
            parent.spawn_scene(lane);
        });

        let right_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 1.422, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("right gutter")
        )).with_children(|parent| {
            parent.spawn_scene(right_gutter);
        });

        let ball_return = asset_server.load("ball_return/ball_return.glb#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 1.657, 0.014, 0.0),
            GlobalTransform::identity(),
            Name::new("ball return")
        )).with_children(|parent| {
            parent.spawn_scene(ball_return);
        });

        // next lane - left gutter
        let next_left_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 1.892, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("next left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(next_left_gutter);
        });

        // next lane
        let next_lane = asset_server.load("lane/lane.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 2.54, 0.0, 0.0),
            GlobalTransform::identity(),
            RigidBody::Static,
            CollisionShape::Cuboid { 
                half_extends: Vec3::new(0.53, 0.028, 9.58),
                border_radius: None,
            },
            PhysicMaterial {
                restitution: 0.5,
                ..Default::default()
            },
            Name::new("next lane")
        )).with_children(|parent| {
            parent.spawn_scene(next_lane);
        });

        // next lane - right gutter
        let next_right_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
        commands.spawn_bundle((
            Transform::from_xyz(x_origin + 3.187, -0.01, 0.0),
            GlobalTransform::identity(),
            Name::new("next left gutter")
        )).with_children(|parent| {
            parent.spawn_scene(next_right_gutter);
        });

        x_origin = x_origin - lane_pattern_size;
    }

}

fn setup_player_lane(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>
) {
    let left_gutter_shim = asset_server.load("gutter_shim/gutter_shim.glb#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(0.0, 0.039, 0.0),
        GlobalTransform::identity(),
        Name::new("player left gutter shim")
    )).with_children(|parent| {
        parent.spawn_scene(left_gutter_shim);
    });

    let left_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(0.127, -0.01, 0.0),
        GlobalTransform::identity(),
        Name::new("left gutter")
    )).with_children(|parent| {
        parent.spawn_scene(left_gutter);
        
        // Left collider
        parent.spawn_bundle((
            Transform::from_xyz(-0.147, -0.023, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // bottom left
        parent.spawn_bundle((
            Transform::from_xyz(-0.076, -0.061, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // bottom
        parent.spawn_bundle((
            Transform::from_xyz(0.0, -0.071, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // bottom right
        parent.spawn_bundle((
            Transform::from_xyz(0.076, -0.061, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // right
        parent.spawn_bundle((
            Transform::from_xyz(0.147, -0.023, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));
    });


    let lane = asset_server.load("lane/lane.gltf#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(0.775, 0.0, 0.0),
        GlobalTransform::identity(),
        RigidBody::Static,
        CollisionShape::Cuboid { 
            half_extends: Vec3::new(0.53, 0.028, 9.58),
            border_radius: None,
        },
        PhysicMaterial {
            restitution: 0.5,
            ..Default::default()
        },
        Name::new("player Lane")
    )).with_children(|parent| {
        parent.spawn_scene(lane);
    });

    let right_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(1.422, -0.01, 0.0),
        GlobalTransform::identity(),
        Name::new("player right gutter")
    )).with_children(|parent| {
        parent.spawn_scene(right_gutter);
        
        // Left collider
        parent.spawn_bundle((
            Transform::from_xyz(-0.147, -0.023, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // bottom left
        parent.spawn_bundle((
            Transform::from_xyz(-0.076, -0.061, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // bottom
        parent.spawn_bundle((
            Transform::from_xyz(0.0, -0.071, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // bottom right
        parent.spawn_bundle((
            Transform::from_xyz(0.076, -0.061, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));

        // right
        parent.spawn_bundle((
            Transform::from_xyz(0.147, -0.023, 0.0)
                .with_rotation(Quat::from_rotation_x(1.57)),
            GlobalTransform::identity(),
            RigidBody::Static,
            PhysicMaterial {
                friction: 0.0,
                density: 1.0,
                restitution: 0.1,
                ..Default::default()
            },
            CollisionShape::Cylinder {
                half_height: 9.58,
                radius: 0.05,
            },
        ));
    });

    let ball_return = asset_server.load("ball_return/ball_return.glb#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(1.657, 0.014, 0.0),
        GlobalTransform::identity(),
        RigidBody::Static,
        CollisionShape::Cuboid {
            half_extends: Vec3::new(0.117, 0.072, 19.2),
            border_radius: None,
        },
        Name::new("player ball return")
    )).with_children(|parent| {
        parent.spawn_scene(ball_return);
    });

    // next lane - left gutter
    let next_left_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(1.892, -0.01, 0.0),
        GlobalTransform::identity(),
        Name::new("player next left gutter")
    )).with_children(|parent| {
        parent.spawn_scene(next_left_gutter);
    });

    // next lane
    let next_lane = asset_server.load("lane/lane.gltf#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(2.54, 0.0, 0.0),
        GlobalTransform::identity(),
        Name::new("player next lane")
    )).with_children(|parent| {
        parent.spawn_scene(next_lane);
    });

    // next lane - right gutter
    let next_right_gutter = asset_server.load("gutter/gutter.gltf#Scene0");
    commands.spawn_bundle((
        Transform::from_xyz(3.187, -0.01, 0.0),
        GlobalTransform::identity(),
        Name::new("player next right gutter")
    )).with_children(|parent| {
        parent.spawn_scene(next_right_gutter);
    });
}


fn setup_lighting(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    asset_server: Res<AssetServer>
) {
    
    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..Default::default()
        },
        transform: Transform::from_xyz(4.0, 8.0, 4.0),
        ..Default::default()
    });

    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..Default::default()
        },
        transform: Transform::from_xyz(0.0, 8.0, -13.0),
        ..Default::default()
    });

    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..Default::default()
        },
        transform: Transform::from_xyz(0.0, 8.0, 8.0),
        ..Default::default()
    });

    commands.spawn_bundle(PointLightBundle {
        point_light: PointLight {
            intensity: 1500.0,
            shadows_enabled: true,
            ..Default::default()
        },
        transform: Transform::from_xyz(0.0, 8.0, 0.0),
        ..Default::default()
    });
}

